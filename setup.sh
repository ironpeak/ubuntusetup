#!/bin/bash

# set -e

# Preconditions
if [[ $EUID -eq 0 ]]; then
   echo "This script must not run as root" 1>&2
   exit 1
fi

# Setup
mkdir -p ~/Apps
mkdir -p ~/Projects

# Installing
sudo apt-get -y update
sudo apt-get -y upgrade

# Chrome
sudo apt-get -y install libxss1 libappindicator1 libindicator7
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome*.deb
rm google-chrome*.deb

# Git
sudo apt-get -y install git

git config --global user.email "HrafnOrri1207@gmail.com"
git config --global user.name "IronPeak"

if [ ! -f ~/.ssh/id_rsa ]
  then
    ssh-keygen -t rsa -b 4096 -C "hrafnorri1207@gmail.com"
    eval "$(ssh-agent -s)"
    ssh-add ~/.ssh/id_rsa
    sudo apt-get -y install xclip
    xclip -sel clip < ~/.ssh/id_rsa.pub
    gedit ~/.ssh/id_rsa.pub
fi

# NodeJS & NPM
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get -y install -y build-essential
sudo apt-get -y install -y nodejs

# Atom
sudo add-apt-repository -y ppa:webupd8team/atom
sudo apt-get update
sudo apt-get -y install atom

# Flatpak
sudo add-apt-repository -y ppa:alexlarsson/flatpak -y
sudo apt-get -y update
sudo apt-get -y install flatpak
wget https://sdk.gnome.org/keys/gnome-sdk.gpg
flatpak remote-add --gpg-import=gnome-sdk.gpg gnome https://sdk.gnome.org/repo/
flatpak install gnome org.freedesktop.Platform
rm gnome-sdk.gpg

# MonoDevelop
wget https://download.mono-project.com/monodevelop/monodevelop-6.1.1.15-2.flatpak
flatpak install --user --bundle monodevelop-6.1.1.15-2.flatpak
rm monodevelop-6.1.1.15-2.flatpak
cp -rf monodevelop.desktop ~/Apps/
chmod +x ~/Apps/monodevelop.desktop

# Franz
sudo rm -rf /opt/franz
sudo rm -rf /usr/share/applications/franz.desktop
sudo mkdir -p /opt/franz
wget -qO- https://github.com/meetfranz/franz-app/releases/download/4.0.4/Franz-linux-x64-4.0.4.tgz | sudo tar xvz -C /opt/franz/
cp -rf franz.desktop ~/Apps/
chmod +x ~/Apps/franz.desktop

# Redshift
sudo apt-get -y install redshift

# Uninstall Stock Apps
sudo apt-get -y remove unity-webapps-common
sudo apt-get -y remove --purge libreoffice*
sudo apt-get -y remove firefox
sudo apt-get -y clean
sudo apt-get -y autoremove

# Configure System
unity-control-center display
software-properties-gtk --open-tab=4
